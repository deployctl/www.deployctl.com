---
title: "Introducing PlantUML for jekyll"
author: Danny Goossen
tags: plantUML, jekyll, deployctl, gitlab, CI, static web deployment
---
## PlantUML 

PlantUML is a powerfull tool to produce Unified Modeling Language diagrams.
With the Jekyll plugin, the uml data is converted into a static svg or png file.

Some examples:


### Bob and Alice

{% plantuml %}
Bob -> Alice : hello
Bob <- Alice : FY2
{% endplantuml %}

## Timing

### timing1

{% plantuml %}
robust "Web Browser" as WB
concise "Web User" as WU

WB is Initializing
WU is Absent

@WB
0 is idle
+200 is Processing
+100 is Waiting
WB@0 <-> @50 : {50 ms lag}

@WU
0 is Waiting
+500 is ok
@200 <-> @+150 : {150 ms}
{% endplantuml %}

#### timing 2

{% plantuml %}
robust "DNS Resolver" as DNS
robust "Web Browser" as WB
concise "Web User" as WU

@0
WU is Idle
WB is Idle
DNS is Idle

@+100
WU -> WB : URL
WU is Waiting
WB is Processing

@+200
WB is Waiting
WB -> DNS@+50 : Resolve URL

@+100
DNS is Processing

@+300
DNS is Idle
{% endplantuml %}

### Activity

{% plantuml %}

start
:ClickServlet.handleRequest();
:new page;
if (Page.onSecurityCheck) then (true)
  :Page.onInit();
  if (isForward?) then (no)
    :Process controls;
    if (continue processing?) then (no)
      stop
    endif
    
    if (isPost?) then (yes)
      :Page.onPost();
    else (no)
      :Page.onGet();
    endif
    :Page.onRender();
  endif
else (false)
endif

if (do redirect?) then (yes)
  :redirect process;
else
  if (do forward?) then (yes)
    :Forward request;
  else (no)
    :Render page template;
  endif
endif

stop
{% endplantuml %}


## Author

Shiori is written by Danny

![](https://gitlab.gioxa.com/uploads/user/avatar/2/danny_pro.jpg)

